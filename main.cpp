﻿#include <iostream>
#include <sstream>
#include <cassert>
#include <conio.h>
#include <vector>
#include <Windows.h>
#define HEAD coords_[0]

class Random {
public:
	Random() : first_value_(0), last_value_(0) { gen(); }
	Random(int bottom, int top) : first_value_(bottom), last_value_(top) {
		assert(bottom <= top);
		gen();
	}

	int get_first_value() const {
		return first_value_;
	}

	int get_last_value() const {
		return last_value_;
	}

	int &set_first_value() {
		return first_value_;
	}

	int &set_last_value() {
		return last_value_;
	}

	int get_last_random_value() const {
		return last_random_value_;
	}

	int gen() {
		last_random_value_ = first_value_ + rand() % (last_value_ - first_value_);
		return last_random_value_;
	}

private:
	int first_value_;
	int last_value_;
	int last_random_value_;
};

class Point {
public:
	Point() : x_(0), y_(0) {}
	Point(size_t x, size_t y) : x_(x), y_(y) {}

	size_t x() const {
		return x_;
	}

	size_t y() const {
		return y_;
	}

	size_t &x() {
		return x_;
	}

	size_t &y() {
		return y_;
	}

	Point &operator%=(const Point &p) {
		x_ %= p.x_;
		y_ %= p.y_;
		return *this;
	}

	Point &random(size_t x_bot, size_t x_top, size_t y_bot, size_t y_top) {
		Random rx(x_bot, x_top);
		x_ = rx.gen();
		Random ry(y_bot, y_top);
		y_ = ry.gen();
		return *this;
	}

private:
	size_t x_;
	size_t y_;
};

bool operator==(Point const &a, Point const &b) {
	if (a.x() == b.x() && a.y() == b.y())
		return true;
	return false;
}

bool operator!=(Point const &a, Point const &b) {
	return !(a == b);
}

const int dx[4] = { 0, 1, 0, -1 };
const int dy[4] = { -1, 0, 1, 0 };

enum Direction { UP, RIGHT, DOWN, LEFT };

class Snake {
public:
	explicit Snake(size_t field_width, size_t field_height) 
		: length_(1), right_down_corner_(field_height - 1, field_height - 1) {
		coords_.push_back(Point().random(0, field_width - 1, 0, field_height - 1));
	}

	Snake &operator++() {
		coords_.push_back(coords_[length_ - 1]);
		++length_;
		return *this;
	}

	Point operator[](const size_t i) const {
		return coords_[i];
	}

	size_t length() const {
		return length_;
	}

	bool is_part(const Point &p) {
		for (int i = 0; i < length_; ++i)
			if (coords_[i] == p)
				return true;
		return false;
	}

	bool move(Direction dir) {
		for (int i = coords_.size() - 1; i > 0; --i) {
			coords_[i] = coords_[i - 1];
		}

		HEAD.x() += dx[dir];
		HEAD.y() += dy[dir];

		HEAD.x() = HEAD.x() & right_down_corner_.x(); //Такой костыль,
		HEAD.y() = HEAD.y() & right_down_corner_.y(); //работает только для полей с высотой и шириной равными степени двойки

		for (int i = 1; i < length_; ++i) {
			if (HEAD == coords_[i])
				return false;
		}

		return true;
	}

private:
	size_t length_;
	std::vector<Point> coords_;
	Point right_down_corner_; //ВВеден только из-за вычитания (или как это вообще назвать?) по модулю
};

char &get_point(char **arr_2d, Point const &p) {
	return arr_2d[p.y()][p.x()];
}

class Field {
public:
	Field(size_t width, size_t height) : width_(width), height_(height), step_count_(0) {
		python_ = new Snake(width, height);
		new_berry();
		field_ = new char*[height];
		field_[0] = new char[width * height];

		for (int i = 1; i < height; ++i) {
			field_[i] = field_[0] + i * width;
		}

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				field_[y][x] = '.';
			}
		}
	}

	~Field() {
		delete python_;
		delete[] field_[0];
		delete[] field_;
	}

	void new_berry() {
		while (true) {
			berry_ = Point().random(0, width_ - 1, 0, height_ - 1);
			bool berry_on_the_field = true;
			for (int i = 0; i < python_->length(); ++i) {
				if ((*python_)[i] == berry_) {
					berry_on_the_field = false;
					break;
				}
			}
			if (berry_on_the_field)
				break;
		}
	}

	bool next_step(Direction dir) {
		++step_count_;
		if (!python_->move(dir))
			return false;
		if ((*python_)[0] == berry_) {
			new_berry();
			++(*python_);
		}
		return true;
	}

	size_t step_count() const {
		return step_count_;
	}

	void draw() {
		static Point last_tail = (*python_)[python_->length() - 1];
		if (last_tail != (*python_)[python_->length() - 1])
			get_point(field_, last_tail) = '.';
		get_point(field_, (*python_)[0]) = '#';
		last_tail = (*python_)[python_->length() - 1];
		get_point(field_, berry_) = '*';
		std::stringstream ss;
		for (int y = 0; y < height_; ++y) {
			for (int x = 0; x < width_; ++x) {
				ss << field_[y][x] << ' ';
			}
			ss << '\n';
		}
		ss << "length: " << python_->length() << " step_count: " << step_count_ << '\n';
		system("Cls");
		fwrite(ss.str().c_str(), sizeof(char), ss.str().size(), stdout);
		ss.clear();
	}

private:
	size_t width_;
	size_t height_;
	size_t step_count_;
	Point berry_;
	Snake *python_;
	char **field_;
};

int main() {
	Field f(8, 8);
	bool gamestate;
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(NULL);
	std::cout << "Press W, A, S or D to start" << std::endl;
	int key = _getch();
	f.draw();
	while (true) {
		Sleep(100);
		if (_kbhit()) {
			key = _getch();
		}
		switch (key)
		{
		case 97:
			gamestate = f.next_step(LEFT);
			break;
		case 119:
			gamestate = f.next_step(UP);
			break;
		case 100:
			gamestate = f.next_step(RIGHT);
			break;
		case 115:
			gamestate = f.next_step(DOWN);
			break;
		default:
			continue;
		}
		f.draw();
		if (!gamestate)
			break;
	}
	std::cout << "Game over" << std::endl;
	system("pause");
}